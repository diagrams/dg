% desenhando 
\documentclass[12pt,a5paper,oneside]{memoir}
\settypeblocksize{.6\stockheight}{.8\stockwidth}{*}
\setlrmargins{*}{*}{1}
\checkandfixthelayout

% Typographic improvement, not necessary for the example,
% but good to include, nonetheless.
\usepackage{microtype} 

\usepackage{tikz} % Pacote para desenhar
\usetikzlibrary{calc} % For coordinate computing
% -----------------------------------------------------
% If you uncomment 3 last lines of this comment, then
% by running latex you'll get pages containing only 
% the contents inside of tikzpicture environments.
%
% That's cool for previewing the results without the
% text around the example, meant to explain the
% construction and discuss usage and limitations of 
% the particular ``drawing'' technique illustrated.
% -----------------------------------------------------
% \usepackage[active,tightpage]{preview} 
% \PreviewEnvironment{tikzpicture}
% \setlength\PreviewBorder{5pt}%

% Give names to some colors.
\colorlet{setborder}{gray}
\colorlet{setfilling}{gray!10}
\colorlet{faint}{gray!70!white}

\begin{document}
\begin{center}


\begin{tikzpicture}
\def \radiusrate {4}
\def \theradius {1cm}
\def \thedistance {2.5cm}
\def \theedgelength {.3}
% Declare coordinates of A's boundary
\begin{scope}[xshift=-\thedistance]
  \foreach \i in {1,...,12}{
    \draw [fill] 
      ($(0,0)!1++1/\radiusrate*rand!\i*360/12:(0,\theradius)$) 
      coordinate (A \i);
};
% Draw the shape A
\draw [setborder,fill=setfilling,very thick] plot[smooth cycle] coordinates{(A 1) (A 2) (A 3) (A 4) (A 5) (A 6) (A 7) (A 8) (A 9) (A 10) (A 11) (A 12)} coordinate (A);
\end{scope}

% Declare coordinates of B's boundary
\begin{scope}[xshift=\thedistance]
  \foreach \i in {1,...,12}{
    \draw [fill] 
        ($(0,0)!1++1/\radiusrate*rand!\i*360/12:(0,\theradius)$) 
        %node [above] {\i}
        coordinate (B \i);
  };

% Draw the shape B
\draw [setborder,fill=setfilling,very thick] plot[smooth cycle] coordinates{(B 1) (B 2) (B 3) (B 4) (B 5) (B 6) (B 7) (B 8) (B 9) (B 10) (B 11) (B 12)} coordinate (B);
\end{scope}


% Declare outside vertice coordinates
\coordinate (upper v) at (  60: \theedgelength);
\coordinate (lower v) at (-120: \theedgelength);


% Node wrapping the edge
%\node (supernode) [fill=white,draw=setborder,circle,inner sep={(2\theedgelength +1mm)}] {};
\node [fill=white,draw=setborder,circle,minimum size={2*\theedgelength cm + 2mm},label=above:{\(v_f\)}] {};


% Connect extremes of edge
\draw [thick,shorten >=2pt, shorten <=2pt] (upper v) -- (lower v);

% Connect vercices to A
\begin{scope}[thick, shorten >=3pt,shorten <=3pt]
  \fill [setfilling] (upper v) -- (A 10) -- (A 9) -- cycle;
  \fill [setfilling] (lower v) -- (A 8) -- (A 7) -- cycle;

  \draw [setborder] (upper v) -- (A 10);
  \draw [setborder] (upper v) --  (A 9);

  \draw [setborder] (lower v) --  (A 8);
  \draw [setborder] (lower v) --  (A 7);

  \fill [setfilling] (upper v) -- (B 2) -- (B 3) -- cycle;
  \fill [setfilling] (lower v) -- (B 4) -- (B 5) -- cycle;

  \draw [setborder] (upper v) --   (B 2);
  \draw [setborder] (upper v) --   (B 3);

  \draw [setborder] (lower v) --   (B 4);
  \draw [setborder] (lower v) --   (B 5);
\end{scope}

% Paint the 2 vertices
\draw [fill] (upper v) circle (1.5pt);
\draw [fill] (lower v) circle (1.5pt);

\end{tikzpicture}
\end{center}

\newcommand{\ie}{\textit{i.e.}}
\newcommand{\mysection}[1]{\plainbreak{1}\noindent\textsc{{\centering\color{red!80!blue}#1}}\par\noindent\ignorespaces}
\newcommand{\code}[1]{\small\texttt{\color{yellow!50!red}#1}} %
 
\mysection{What} We are to build an imperfect circle-like shape, somewhat mimicking a set.

\mysection{How} We will draw evenly (degree) spaced points in a circle, and add a small perturbation in their position. If you think of the polar-coordinate system representation of the points, then the perturbation is going to be at the \emph{radius} rather than the angle of each coordinate.

\mysection{Why this way?} One of the major concerns behind this construction is that ``simplier'' strategies tend to yield sets whose shape is ``regular'' in some sense (\ie, circles, elipses). But why should that be a concern? Well, oftentimes one is drawing in search of  some unavoidable property of an abstract object. When drawing, though, it is common that some accidental ``regularities'' creep in, as a result of non-conscious placing of nodes, drawing of edges, or simply bad luck, and those may lure us into taking for general patterns which, put simply, aren't. Therefore the widespread preference for irregular and wiggly, shapes and function-graph lines, as if to stress the arbitrariness of the representation (and thus making it harder on our inconsciousness to take some hypothesis for granted).

Our goal is draw a smoot line passing through some points. Such a thing is achievable using the \code{plot [smooth cycle] coordinates} operation on a path.


\begin{center}
  \begin{tikzpicture}
    \begin{scope}[gray!40!white] % Painting the fitted coordinates
      \coordinate (A) at ( 0, 0); \fill (A) circle (2pt);
      \coordinate (B) at ( 1, 0); \fill (B) circle (2pt);
      \coordinate (C) at (.5,.5); \fill (C) circle (2pt);
      \coordinate (D) at ( 3, 1); \fill (D) circle (2pt);
      \coordinate (E) at ( 0, 1); \fill (E) circle (2pt);
    \end{scope}

    \draw plot [smooth cycle] coordinates { (A) (B) (C) (D) (E) };
  \end{tikzpicture}
\end{center}

As in the previous example, we'll need to declare the coordinates of the points ``fitted'' in the boundary of the shape. As a good practice, we make the declaration of the coordinates into a loop, so if we want more points it'll be easier to add them later.



\begin{center}
  \begin{tikzpicture}
    \def \radiusrate {10}
    \def \theradius {1cm}
    \foreach \i in {1,...,12} {
      \coordinate (A \i) at 
      ($(0,0)!1++1/\radiusrate!\i*360/12:(0,\theradius)$);
%      ($(0,0)!1++1/\radiusrate!\i*360/12:(0,\theradius)$);
  };

  \foreach \i in {1,...,12}{
    \fill [fill=faint] (A \i) circle (2pt);
};
  \end{tikzpicture}
\end{center}


\begin{center}
\begin{tikzpicture}
\def \n{30}
\def \radiusrate {5};
\def \theradius {2cm};
\foreach \i in {1,...,\n} {
  \coordinate (A \i) at
  ($(0,0)!{1 + .2*rand}!({\i*360/\n}:\theradius)$);
};

% This has to be done by hand
\draw [fill=faint,line width=4pt, draw=setborder] plot [smooth cycle] coordinates
{(A 1) (A 2) (A 3) (A 4) (A 5) (A 6) (A 7) (A 8) (A 9) (A 10) (A 11) (A 12) (A 13) (A 14) (A 15) (A 16) (A 17) (A 18) (A 19) (A 20) (A 21) (A 22) (A 23) (A 24) (A 25) (A 26) (A 27) (A 28) (A 29) (A 30)};

\foreach \i in {1,...,\n} {
  \fill [black] (A \i) circle (2pt);
%  \draw [dashed, shorten <=2pt,shorten >=2pt] (0,0) -- (A \i);
};

% We display the maximum and minimum radius of the dots
\draw [dotted] (0,0) circle (\theradius);
\draw [dotted] (0,0) circle (.8*\theradius);
\draw [dotted] (0,0) circle (1.2*\theradius);
\end{tikzpicture}
\end{center}

\begin{center}
\begin{tikzpicture}
\def \n{10}
\def \radiusrate {5};
\def \theradius {2cm};
\foreach \i in {1,...,\n} {
  \coordinate (A \i) at
  ($(0,0)!{1 + .2*rand}!({\i*360/\n}:\theradius)$);
};

% This has to be done by hand
\draw [fill=faint,line width=4pt, draw=setborder] plot [smooth cycle] coordinates
{(A 1) (A 2) (A 3) (A 4) (A 5) (A 6) (A 7) (A 8) (A 9) (A 10)};

\foreach \i in {1,...,\n} {
  \fill [black] (A \i) circle (2pt);
%  \draw [dashed, shorten <=2pt,shorten >=2pt] (0,0) -- (A \i);
};

% We display the maximum and minimum radius of the dots
\draw [dotted] (0,0) circle (\theradius);
\draw [dotted] (0,0) circle (.8*\theradius);
\draw [dotted] (0,0) circle (1.2*\theradius);
\end{tikzpicture}
\end{center}

\end{document}
